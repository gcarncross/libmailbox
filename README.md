# libmailbox

This is a simple utility for sending messages to the main loop (deferred).

It exposes a single API:

    .com_kx_mailbox.send(msg)

that does what some people want 0(msg) to do (but doesn't).

    q).com_kx_mailbox.send(value;"0N!`dude");0N!`first;
    `first
    q)`dude


